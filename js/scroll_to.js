( $ => {

     $('nav ul, .nav-list, .short-info').on('click', 'a', function(event) {
          event.preventDefault();
          var 
               id = $(this).attr('href'),
               height = $(id).offset().top;

          $('body, html').animate({scrollTop: height}, 1500);

          if ( $(this).attr('href') === '#actions' )
               $('#order input:first-of-type').focus()
     })

})(jQuery)